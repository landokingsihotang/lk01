public class Tiket {
    private String namaPenumpang;
    private String asal;
    private String tujuan;

    //membuat constuctor overloading

    //constructor untuk data tiket penumpang kereta komuter
    public Tiket(String namaPenumpang){
        this.namaPenumpang = namaPenumpang;
    }
    
    //constructor untuk data tiket penumpang kereta KAJJ
    public Tiket(String namaPenumpang, String asal, String tujuan) {
        this.namaPenumpang = namaPenumpang;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    // method mengakses nama penumpang kereta komuter ataupun KAJJ
    public String getNamaPenumpang() {
        return namaPenumpang;
    }
    
    // method mengakses asal penumpang kereta KAJJ
    public String getAsal() {
        return asal;
    }
    
    // method mengakses tujuan penumpang kereta KAJJ
    public String getTujuan() {
        return tujuan;
    }
}
