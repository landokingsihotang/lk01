public class Kereta {
    private String namaKereta;
    private int tiketTersedia;//akan menjadi index dari array Tiket
    private int jumlahPenumpang;
    private Tiket[] daftarTiket;//membuat array dengan tipe data Tiket
   
    //membuat constructor overloading
    //default constructor untuk Kerata Komuter 
    public Kereta() {
        namaKereta = "Komuter";
        tiketTersedia = 1000;//jumlah tiket komuter
        daftarTiket = new Tiket[tiketTersedia];//menginisiasi array tempat data penumpang
    }
    //constructor untuk kereta KAJJ dengan parameter
    public Kereta(String namaKereta, int tiketTersedia) {
        this.namaKereta = namaKereta;
        this.tiketTersedia = tiketTersedia;//jumlah tiket KAJJ
        daftarTiket = new Tiket[this.tiketTersedia];//menginisiasi array tempat data penumpang
    }

    //method untuk menambahkan tiket
    //membuat method overloading dengan parameter nama untuk kereta komuter
    //data penumpang yang diinput akan ditampung dalam array "Tiket"
    public void tambahTiket(String namaPenumpang) {
        if (jumlahPenumpang < tiketTersedia) {
            Tiket tiket = new Tiket(namaPenumpang);
            daftarTiket[jumlahPenumpang] = tiket;
            jumlahPenumpang++;
            System.out.println("========================================================");
            //apabila tiket tersisa kurang dari
            //sistem akan menampilkan tiket tersedia
            if ((tiketTersedia-jumlahPenumpang)>=30){
                System.out.println("Tiket berhasil dipesan");
            }
            else
            if((tiketTersedia-jumlahPenumpang)<30) {System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: "+(tiketTersedia-jumlahPenumpang));
        }
    } 
    //tampilan ,apabila tiket telah habis
        else {
            System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }
    
    //method untuk menambahkan tiket
    //membuat method overloading dengan parameter namaPenumpang,asal ,dan tujuan penumpang untuk kereta komuter
    //data penumpang yang diinput akan ditampung dalam array "Tiket"
    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
        if (jumlahPenumpang < tiketTersedia) {
            Tiket tiket = new Tiket(namaPenumpang, asal, tujuan);
            daftarTiket[jumlahPenumpang] = tiket;
            jumlahPenumpang++;
            System.out.println("========================================================");
            //apabila tiket tersisa 30 atau dibawahnya 
            //sistem akan menampilkan tiket tersedia
            if ((tiketTersedia - jumlahPenumpang) >= 30) {
                System.out.println("Tiket berhasil dipesan");
            } else if ((tiketTersedia - jumlahPenumpang) < 30) {
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + (tiketTersedia - jumlahPenumpang));
            }
        } 
         //tampilan ,apabila tiket telah habis
        else {
            System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //menampilan tiket yang dipesan oleh penumpang
    //ketika sistem mendeteksi tiket tak memiliki asal 
    //maka sistem akan mencetak tiket untuk penumpang kereta komuter
    //apabila sistem mendeteksi adanya asal penumpang
    //maka sistem akan mencetak tiket untuk penumpang kereta KAJJ
    public void tampilkanTiket() {
        System.out.println("========================================================");
        System.out.println("Tiket untuk kereta api " + namaKereta + ":");
        System.out.println("-----------------------------");
        for (int a = 0; a < jumlahPenumpang; a++) {
            //Tiket kereta KAJJ
            if (daftarTiket[a].getAsal() != null) {
            System.out.println("Nama\t: " + daftarTiket[a].getNamaPenumpang());
            System.out.println("Asal\t: " + daftarTiket[a].getAsal());
            System.out.println("Tujuan\t: " + daftarTiket[a].getTujuan());
            System.out.println("-----------------------------");}
                
            //Tiket kereta Komuter
        else {
            System.out.println("Nama\t: " + daftarTiket[a].getNamaPenumpang());
        }

        }
    }
}
